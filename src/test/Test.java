import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Test {

	@org.junit.jupiter.api.Test
	void test() throws IOException {
		String path = "C:\\Users\\matbo\\Documents\\IFT3913\\GIT"
				+ "\\tp1_ift3913\\jfreechart\\src\\main\\java\\org"
				+ "\\jfree\\chart\\annotations\\Annotation.java";
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		Stream<String> language = Files.lines(new File(path).toPath())
				.map(fileBeforeTrim -> fileBeforeTrim.trim())
				.filter(fileBeforeTrim -> !fileBeforeTrim.isEmpty());
		List<String> prog = language.collect(Collectors.toList());
		
		TP1 test = new TP1();
		
		test_Classe_LOC(prog,test);
		test_Classe_CLOC(prog,test);
		test_Classe_DC(prog,test);
		test_Classe_WMC(prog,test);
		test_Classe_BC(prog,test);
		test_Classe_Desc(prog,path,test);
		test_getName(prog,test);
		
		test_Method_LOC(prog,test);
		test_Method_CLOC(prog,test);
		test_Method_DC(prog,test);
		test_Method_CC(prog,test);
		test_Method_BC(prog,test);
		test_Method_Desc(prog,path,test);
		
	}
	
	void test_Classe_LOC(List<String> prog,TP1 test) {
		int classe_LOC = test.classe_LOC(prog);
		assertEquals(59,classe_LOC, "classe_LOC failed");
	}
	
	void test_Classe_CLOC(List<String> prog,TP1 test) {
		int classe_CLOC = test.classe_CLOC(prog);
		assertEquals(52,classe_CLOC, "classe_CLOC failed");
	}
	
	void test_Classe_DC(List<String> prog,TP1 test) {
		int classe_LOC = test.classe_LOC(prog);
		int classe_CLOC = test.classe_CLOC(prog);
		double classe_DC = test.classe_DC(classe_LOC, classe_CLOC);
		assertEquals(52.0/59.0,classe_DC, "classe_DC failed");
	}
	
	void test_Classe_WMC(List<String> prog,TP1 test) {
		ArrayList<Double> method_CC = test.methode_CC(prog);
		double classe_WMC = test.classe_WMC(method_CC);
		assertEquals(2.0,classe_WMC, "classe_WMC not equal");
	}
	
	void test_Classe_BC(List<String> prog,TP1 test) {
		int classe_LOC = test.classe_LOC(prog);
		int classe_CLOC = test.classe_CLOC(prog);
		double classe_DC = test.classe_DC(classe_LOC, classe_CLOC);
		
		ArrayList<Double> method_CC = test.methode_CC(prog);
		double classe_WMC = test.classe_WMC(method_CC);
		
		double classe_BC = classe_DC/classe_DC;
		assertEquals(2.0,classe_WMC, "classe_BC not equal");
	}
	
	void test_Method_LOC(List<String> prog,TP1 test) {
		ArrayList<Double> method_LOC = test.methode_LOC(prog);
		ArrayList<Double> expected = new ArrayList<Double>();
		expected.add(6.0);
		expected.add(6.0);
		assertEquals(expected,method_LOC, "method_LOC not equal");
	}
	
	void test_Method_CLOC(List<String> prog,TP1 test) {
		ArrayList<Double> method_CLOC = test.methode_CLOC(prog);
		ArrayList<Double> expected = new ArrayList<Double>();
		expected.add(5.0);
		expected.add(5.0);
		assertEquals(expected,method_CLOC, "method_CLOC not equal");
	}
	
	void test_Method_DC(List<String> prog,TP1 test) {
		ArrayList<Double> method_LOC = test.methode_LOC(prog);
		ArrayList<Double> method_CLOC = test.methode_CLOC(prog);
		ArrayList<Double> method_DC = test.methode_DC(method_LOC,method_CLOC);
		ArrayList<Double> expected = new ArrayList<Double>();
		expected.add(5.0/6.0);
		expected.add(5.0/6.0);
		assertEquals(expected,method_DC, "method_DC not equal");

	}
	
	void test_Method_CC(List<String> prog,TP1 test) {
		ArrayList<Double> method_CC = test.methode_CC(prog);
		ArrayList<Double> expected = new ArrayList<Double>();
		expected.add(1.0);
		expected.add(1.0);
		assertEquals(expected,method_CC, "method_CC not equal");

	}
	
	void test_Method_BC(List<String> prog,TP1 test) {
		ArrayList<Double> method_LOC = test.methode_LOC(prog);
		ArrayList<Double> method_CLOC = test.methode_CLOC(prog);
		ArrayList<Double> method_DC = test.methode_DC(method_LOC,method_CLOC);
		ArrayList<Double> method_CC = test.methode_CC(prog);
		
		ArrayList<Double> method_BC = test.methode_BC(method_DC,method_CC);
		ArrayList<Double> expected = new ArrayList<Double>();
		expected.add(5.0/6.0);
		expected.add(5.0/6.0);
		assertEquals(expected,method_BC, "method_BC not equal");

	}
	
	void test_Classe_Desc(List<String> prog, String path, TP1 test) {
		String expected = "C:\\Users\\matbo\\Documents\\IFT3913\\GIT\\tp1_ift3913\\jfreechart"
				+ "\\src\\main\\java\\org\\jfree\\chart\\annotations\\Annotation.java,"
				+ "Annotation,59,52,0.8813559322033898,2.0,0.4406779661016949";
		String resultat = test.class_Desc(path, prog);
		assertEquals(expected,resultat, "classe_Desc failed");
	}
	
	void test_Method_Desc(List<String> prog, String path, TP1 test) {
		//String expected = "C:\\Users\\matbo\\Documents\\IFT3913\\GIT\\tp1_ift3913\\jfreechart"
			//	+ "\\src\\main\\java\\org\\jfree\\chart\\annotations\\Annotation.java,"
				//+ "Annotation,59,52,0.8813559322033898,2.0,0.4406779661016949";
		String resultat = test.methode_Desc(path, prog);
		String expected = "\r\n" + 
				"C:\\Users\\matbo\\Documents\\IFT3913\\GIT\\tp1_ift3913\\jfreechart\\src\\main\\java"
				+ "\\org\\jfree\\chart\\annotations\\Annotation.java,Annotation,addChangeListener_AnnotationChangeListener,"
				+ "6.0,5.0,0.8333333333333334,1.0,0.8333333333333334\r\n" + 
				"C:\\Users\\matbo\\Documents\\IFT3913\\GIT\\tp1_ift3913\\jfreechart\\src\\main\\java\\org\\jfree\\chart"
				+ "\\annotations\\Annotation.java,Annotation,removeChangeListener_AnnotationChangeListener,6.0,5.0,"
				+ "0.8333333333333334,1.0,0.8333333333333334";
		assertEquals(expected,resultat, "Method_Desc not equal");
	}
	
	void test_getName(List<String> prog,TP1 test) {
		String result = test.getName(prog.get(45));
		assertEquals("Annotation",result, "getName not equal");
	}
}
