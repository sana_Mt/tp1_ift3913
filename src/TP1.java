import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.regex.*;
import java.io.FileWriter;


/**
 * @author Sana Matboui, 20123453
 * @author Yi Cong Li, 20122756
 */
public class TP1 {

	/**
	 * Main method of the program
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String directoryPath = args[0];
		List<File> files = new ArrayList<>();
		addFiles(directoryPath, files);
		List<String> listClasses = new ArrayList<>();
		List<String> listMethodes = new ArrayList<>();

		//reads all the files from the folder and its sub-folders given in args[0]
		//generation of classes.csv
		for (File file : files) {
			if (file.isFile() && file.getName().contains(".java")) {
				System.out.println(file.getName());
				String newPath = file.getAbsolutePath();
				Stream<String> language = Files.lines(new File(newPath).toPath())
					.map(fileBeforeTrim -> fileBeforeTrim.trim())
					.filter(fileBeforeTrim -> !fileBeforeTrim.isEmpty());
				List<String> prog = language.collect(Collectors.toList());
				if(class_Desc(file.getAbsolutePath(), prog) != "") {
					if(directoryPath.contains("\\")){
						directoryPath = directoryPath.substring(directoryPath.lastIndexOf("\\")+1);
					}
					listClasses.add(class_Desc(
							file.getAbsolutePath().substring(file.getAbsolutePath().indexOf(directoryPath)), prog));
				}
			}
		}
		write_CSV(listClasses, "./classes.csv");

		//generation of methodes.csv
		for (File file : files) {
			if (file.isFile() && file.getName().contains(".java")) {
				System.out.println(file.getName());
				String newPath = file.getAbsolutePath();
				Stream<String> language = Files.lines(new File(newPath).toPath())
						.map(fileBeforeTrim -> fileBeforeTrim.trim())
						.filter(fileBeforeTrim -> !fileBeforeTrim.isEmpty());
				List<String> prog = language.collect(Collectors.toList());
				listMethodes.add(methode_Desc(
						file.getAbsolutePath().substring(file.getAbsolutePath().indexOf(directoryPath)), prog));
			}
		}
		write_CSVMethode(listMethodes, "./methodes.csv");
	}

	/**
	 * Help to navigate all the sub-folders of the folder in order
	 * to scan all files of the folder
	 * @param directoryPath path of the folder to scan
	 * @param files list of accumulated files
	 */
	public static void addFiles(String directoryPath, List<File> files){
		File directory = new File(directoryPath);
		File[] listOfFiles = directory.listFiles();
		if(listOfFiles != null){
			for(File file : listOfFiles){
				if(file.isFile()){
					files.add(file);
				} else if (file.isDirectory()){
					addFiles(file.getAbsolutePath(), files);
				}
			}
		}
	}
	
	/**
	 * @param list	lines to write in the csv file
	 * @param fileName name of the csv file
	 */
	public static void write_CSV(List<String> list,String fileName){
		File output = new File(fileName);
		
		try { 
			FileWriter writer = new FileWriter(output);
			
	        writer.write("chemin, class, class_LOC, class_CLOC, class_DC, WMC, classe_BC\n");
			for(int i = 0; i < list.size(); i++){
				writer.write(list.get(i));
			}
	        writer.close(); 
	    } 
	    catch (IOException e) {  
	        e.printStackTrace(); 
	    } 
	}

	/**
	 * @param list	lines to write in the csv file
	 * @param fileName name of the csv file
	 */
	public static void write_CSVMethode(List<String> list,String fileName){
		File output = new File(fileName);

		try {
			FileWriter writer = new FileWriter(output);

			writer.write("chemin, class, methode, methode_LOC, methode_CLOC, "
					+ "methode_DC, CC, methode_BC\n");
			for(int i = 0; i < list.size(); i++){
				writer.write(list.get(i));
			}
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @param path path of the class we will describe
	 * @param file	list of the lines in the class
	 * @return description of the class
	 */
	public static String class_Desc(String path, List<String> file) {
		int nbLOC = classe_LOC(file);
		int nbCLOC = classe_CLOC(file);
		double ratioDC = classe_DC(nbLOC, nbCLOC);
		double sommeWMC = classe_WMC(methode_CC(file));
		double ratioBC = classe_BC(ratioDC, sommeWMC);
		String name = "";
		
		//Finds line containing name of the class
		for(int i = 0; i <file.size(); i++) {
			String line = file.get(i);
			if(!line.contains("*") && !line.contains("//") && line.contains("public") || line.indexOf("class") == 0) {
				if (line.contains("class") || line.contains("interface")
						|| line.contains("enum") || line.contains("abstract class")) {
					name = getName(line);
					break;
				}
			}
		}
		if(name != "") {
			String description = path + "," + name + "," + nbLOC + "," + nbCLOC
					+ "," + ratioDC + "," + sommeWMC + "," +
					ratioBC + "\n";
			return description;
		} else {
			return  "";
		}
	}

	/**
	 * @param path path of the class containing the mothodes we will describe
	 * @param file	list of the lines in the class
	 * @return description of the methodes
	 */
	public static String methode_Desc(String path, List<String> file) {
		ArrayList<Double> methodeLOC = methode_LOC(file);
		ArrayList<Double> methodeCLOC = methode_CLOC(file);
		ArrayList<Double> methodeDC = methode_DC(methodeLOC, methodeCLOC);
		ArrayList<Double> complextiteCC = methode_CC(file);
		ArrayList<Double> methodeBC = methode_BC(methodeDC, complextiteCC);
		String name = "";

		//Finds line containing name of the class
		for(int i = 0; i <file.size(); i++) {
			String line = file.get(i);
			if ((line.contains("class") || line.contains("interface"))
					&& line.charAt(line.length() -1) == '{') {
				name = getName(line);
				break;
			}
			else if (line.contains("public abstract class")){
				name = getName(line);
				break;
			}
		}

		//Finds line containing name of each methode
		String regex = "(public|protected|private)\\s(([^\\(\\)\\s]+)\\s)"
				+ "+[\\w\\(\\)\\s\\,]+(\\s?\\{|\\;)?";
		ArrayList<String> methodeName = new ArrayList<>();
		for(int i = 0; i < file.size(); i++) {
			String line = file.get(i);
			boolean condition = line.contains("(")
					&& !line.contains(" new ")
					&& line.matches(regex);
			if(condition){// if conditions of methode identification are satisfied
				String[] split = line.split(" ");
				for (int j = 0; j < split.length; j++){
					if(split[j].contains("(")){
						if(split[j].contains(")")) {//if this methode has no argument
							methodeName.add(split[j].substring(0,
									split[j].indexOf("(")));
						} else{//case of the methode with arguments
							String nameWithArguments = split[j].substring(0,
									split[j].indexOf("(")) + "_" +
									split[j].substring(split[j].indexOf("(")+1);
							while(j+2 < split.length && !split[j+1].contains(")")){
								nameWithArguments += "_" + split[j+2];
								j += 2;
							}
							if(!line.contains(")")) {//case of the methode having more than one line of arguments
								while (!line.contains(")")) {
									i++;
									line = file.get(i);
									split = line.split(" ");
									int k = 0;
									nameWithArguments += "_" + split[k];
									while (k + 2 < split.length
											&& !split[k + 1].contains(")")) {
										nameWithArguments += "_" + split[k + 2];
										k += 2;
									}
								}
							}
							methodeName.add(nameWithArguments);
						}
					}
				}
			}
		}
		String description = "";
		//generate description of each methode
		for(int i = 0; i < methodeCLOC.size(); i++) {
			description += path + "," + name + "," + methodeName.get(i)
					+ "," + methodeLOC.get(i) +"," +
					methodeCLOC.get(i) + "," + methodeDC.get(i)
					+ "," + complextiteCC.get(i) + "," +
					methodeBC.get(i) + "\n";
		}
		return description;
	}

	
	/**
	 * @param line line containing the name of the class or method
	 * @return name of the method or class
	 */
	public static String getName (String line){
		String[] split = line.split(" ");
		String name = "";
		for(int i = 0; i+1 < split.length; i++){
			if(split[i].contains("class") || split[i].contains("interface")
			|| split[i].contains("enum")){
				if(!split[i+1].contains("<")) {
					name = split[i + 1];
					break;
				}else {
					name = split[i+1].substring(0,split[i+1].indexOf("<"));
				}
			}
		}
		return name;
	}

	/**
	 * generates LOC of a class
	 * @param prog list of the lines in the programme
	 * @return number of lines in the class
	 */
	public static int classe_LOC(List<String> prog) {
		return prog.size();
	}

	/**
	 * generates CLOC of a class
	 * @param prog list of the lines in the programme
	 * @return number of lines of comments in the class
	 */
	public static int classe_CLOC(List<String> prog) {
		int nbCom = 0;
		for(int i = 0; i < prog.size(); i++) {
			
			if(prog.get(i).contains("//")) {
                nbCom++;
			} else if(prog.get(i).contains("/*")) {
				
				while ((i < prog.size()) && !prog.get(i).contains("*/")) {
                    nbCom++;
                    i++;
                }
                nbCom ++;
			}
		}
		return nbCom;
	}

	/**
	 * generate LOC of each methode in a class
	 * @param prog, list of lines of the class
	 * @return list of LOC of all methodes in the class
	 */
	public static ArrayList<Double> methode_LOC(List<String> prog) {
		ArrayList<Double> methodeLineList = new ArrayList<Double>();
		double nbLineMethod = 0;
		for(int i = 0; i < prog.size(); i++){
			int j = i;
			//treat the javadoc before methode
			if(prog.get(i).contains("/**") || prog.get(i).contains("/*")) {
				while (!prog.get(j).contains("*/") && j < prog.size()) {
					j++;
				}
				j++;
				if (prog.get(j).contains("@Override")) {
					j++;
				}
			}
			String regex = "(public|protected|private)\\s(([^\\(\\)\\s]+)\\s)"
					+ "+[\\w\\(\\)\\s\\,]+(\\s?\\{|\\;)?";
			boolean condition = prog.get(j).contains("(")
					&& !prog.get(j).contains(" new ")
					&& prog.get(j).matches(regex);
			if (condition) {// if conditions of methode identification are satisfied
				nbLineMethod += j + 1 - i;
				i = j;
				//case of the methode having more than one line of arguments
				while (!prog.get(j).contains(")")){
					j++;

				}
				while(!prog.get(j).contains("{") && !prog.get(j).contains(";")){
					j++;
				}
				if (prog.get(j).contains("{")) {//treat the brackets
					j++;
					int openBracket = 1;
					while (openBracket != 0 && j < prog.size()) {
						if (prog.get(j).contains("{")) {
							openBracket++;
						}
						if (prog.get(j).contains("}")) {
							openBracket--;
						}
						j++;
					}
					nbLineMethod += j - 1 - i;
					methodeLineList.add(nbLineMethod);
					i = j - 1;
				} else {methodeLineList.add(nbLineMethod);}
				nbLineMethod = 0;
			}else{
				i=j;
				nbLineMethod = 0;
			}
		}
		return methodeLineList;
	}

	/**
	 * generate CLOC of each methode in a class
	 * @param prog, list of lines of the class
	 * @return list of CLOC of all methodes in the class
	 */
	public static ArrayList<Double> methode_CLOC(List<String> prog) {
		ArrayList<Double> methodeCommentList = new ArrayList<Double>();
		double nbCommentMethod = 0;
		for (int i = 0; i < prog.size(); i++) {
			int j = i;
			//treat the javadoc before methode
			if (prog.get(i).contains("/**") || prog.get(i).contains("/*")) {
				while (!prog.get(j).contains("*/") && j < prog.size()) {
					j++;
				}
				j++;
				if (prog.get(j).contains("@Override")) {
					j++;
					nbCommentMethod--;
				}
			}
			String regex = "(public|protected|private)\\s(([^\\(\\)\\s]+)\\s)"
					+ "+[\\w\\(\\)\\s\\,]+(\\s?\\{|\\;)?";
			boolean condition = prog.get(j).contains("(")
					&& !prog.get(j).contains(" new ")
					&& prog.get(j).matches(regex);
			if (condition) {// if conditions of methode identification are satisfied
				nbCommentMethod += j - i;
				if(prog.get(j).contains("//") && !prog.get(j).contains("{")){
					nbCommentMethod++;
				}
				i = j;
				//case of the methode having more than one line of arguments
				while (!prog.get(j).contains(")")){
					if(prog.get(j).contains("//")){
						nbCommentMethod++;
					}
					j++;
				}
				while(!prog.get(j).contains("{") && !prog.get(j).contains(";")){
					if(prog.get(j).contains("//")){
						nbCommentMethod++;
					}
					j++;
				}
				if (prog.get(j).contains("{")) {//treat the brackets
					if(prog.get(j).contains("//")){
						nbCommentMethod++;
					}
					j++;
					int openBracket = 1;
					while (openBracket != 0 && j < prog.size()) {
						if (prog.get(j).contains("{")) {
							openBracket++;
						}
						if (prog.get(j).contains("}")) {
							openBracket--;
						}
						if(prog.get(j).contains("//")){
							nbCommentMethod++;
						}
						j++;
					}
					methodeCommentList.add(nbCommentMethod);
					i = j - 1;
				}else{methodeCommentList.add(nbCommentMethod);}
				nbCommentMethod = 0;
			}else{
				i=j;
				nbCommentMethod = 0;
			}
		}
		return methodeCommentList;
	}


	/**
	 * @param nbLOC number of lines in the class
	 * @param nbCLOC number of comment lines in the class
	 * @return DC
	 */
	public static double classe_DC(int nbLOC, int nbCLOC) {
		double dLOC = nbLOC;
		double dCLOC = nbCLOC;
		return dCLOC/dLOC;
	}

	/**
	 * generate DC of all methodes in a class from their LOC and CLOC
	 * @param methodeLOC, list of LOC of all methodes
	 * @param methodeCLOC, list of CLOC of all methodes
	 * @return list of DC of all methodes
	 */
	public static ArrayList<Double> methode_DC(ArrayList<Double> methodeLOC,
			ArrayList<Double> methodeCLOC) {
		ArrayList<Double> methodeDC = new ArrayList<Double>();
		for(int i = 0; i < methodeCLOC.size(); i++) {
			methodeDC.add(methodeCLOC.get(i)/methodeLOC.get(i));
		}
		return  methodeDC;
	}

	/**
	 * generate CC of each methode in a class
	 * @param prog, list of lines of the class
	 * @return list of CC of all methodes in the class
	 */
	public static ArrayList<Double> methode_CC(List<String> prog){
		ArrayList<Double> methodeCheminList = new ArrayList<Double>();
		double nbChemin = 1;
		for (int i = 0; i < prog.size(); i++) {
			int j = i;
			//Pass the javadoc before methode
			if (prog.get(i).contains("/**") || prog.get(i).contains("/*")) {
				while (!prog.get(j).contains("*/") && j < prog.size()) {
					j++;
				}
				j++;
				if (prog.get(j).contains("@Override")) {
					j++;
				}
			}
			String regex = "(public|protected|private)\\s(([^\\(\\)\\s]+)\\s)"
					+ "+[\\w\\(\\)\\s\\,]+(\\s?\\{|\\;)?";
			boolean condition = prog.get(j).contains("(")
					&& !prog.get(j).contains(" new ")
					&& prog.get(j).matches(regex);
			if (condition) {// if conditions of methode identification are satisfied
				i = j;
				//case of the methode having more than one line of arguments
				while (!prog.get(j).contains(")")){
					j++;
				}
				while(!prog.get(j).contains("{") && !prog.get(j).contains(";")){
					j++;
				}
				if (prog.get(j).contains("{")) {//treat the brackets
					j++;
					int openBracket = 1;
					while (openBracket != 0 && j < prog.size()) {
						if (prog.get(j).contains("{")) {
							openBracket++;
						}
						if (prog.get(j).contains("}")) {
							openBracket--;
						}
						if(prog.get(j).contains("if") || prog.get(j).contains("while")
						|| prog.get(j).contains("case")){
							nbChemin++;
						}
						j++;
					}
					if(j >= prog.size()) {
						j--;
					}
					methodeCheminList.add(nbChemin);
					i = j - 1;
				}else{methodeCheminList.add(nbChemin);}
				nbChemin = 1;
			}else{
				i=j;
				nbChemin = 1;
			}
		}
		return methodeCheminList;
	}

	/**
	 * generate BC of all methodes in a class from their DC and CC
	 * @param methodeDC, list of DC of all methodes
	 * @param methodeCC, list of CC of all methodes
	 * @return list of BC of all methodes
	 */
	public static ArrayList<Double> methode_BC(ArrayList<Double> methodeDC,
			ArrayList<Double> methodeCC) {
		ArrayList<Double> methodeBC = new ArrayList<Double>();
		for(int i = 0; i < methodeDC.size(); i++) {
			methodeBC.add(methodeDC.get(i)/methodeCC.get(i));
		}
		return  methodeBC;
	}

	/**
	 * generate WMC of a class from CC of all methodes in this class
	 * @param methodeCC list of CC of all methodes
	 * @return WMC of class
	 */
	public static double classe_WMC(ArrayList<Double> methodeCC){
		double classWMC = 0;
		for(int i = 0; i < methodeCC.size(); i++){
			classWMC += methodeCC.get(i);
		}
		return classWMC;
	}

	/**
	 * generate BC of a class from its DC and WMC
	 * @param classDC DC of this class
	 * @param classWMC WMC of this class
	 * @return BC of class
	 */
	public static double classe_BC(double classDC, double classWMC) {
		return  classDC/classWMC;
	}
}
