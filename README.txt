Noms: Yi Cong Li et Sana Matboui

S.V.P. Soit utiliser le chemin absolu du ficher qui contient les fichiers JAVA que
vous voulez analyser comme le premier argument, soit mettre ce fichier dans le même fichier
qui contient TP1.jar et utiliser le nom de ce fichier comme premier argument.
Par exemple, si vous voulez analyser "jfreechart", veuillez déplacer au fichier
qui contient TP1.jar et tapper la commande ci-dessous:
java -jar TP1.jar "...\jfreechart" | si "jfreechart" n'est pas dans le même fichier
OU
java -jar TP1.jar "jfreechart" | si "jfreechart" est dans le même fichier
Les noms du fichiers JAVA scannés seront affichés sur votre écran.
Les fichiers CSV du résultat seront générés dans le même fichier qui contient
TP1.jar.

Le fichier "test" dans "src" sert aux tests unitaires.

P.S. Les valeurs "infinity" sont accordées à BC des classes qui n'ont aucune
méthode du tout, vu que une valeur "0" ou "-1" peut donner la confusion que
ces classes sans méthodes sont tous mal commentées, ce qui n'est pas vrai. Vu
qu'elles n'ont pas de méthodes, on compare seulement DC pour ces classes.

Aussi, on avait mis le fichier "jfreechart" dans notre repos au cours de ce
projet. On l'a effacé après avoir su qu'on ne doit pas le mettre, mais ça a
causé des commits qui semblent massives. S.V.P. prendre cela en compte lorsque
vous examinez notre repository.

Merci d'utiliser notre programme!